from datetime import datetime, timedelta
import urllib
import requests
from tqdm import tqdm
from flask import Flask, render_template,request
import json
import time
from collections import defaultdict
from apscheduler.schedulers.background import BackgroundScheduler
import random
import os
import hashlib
from satnogs_api_client import DB_BASE_URL, get_paginated_endpoint,fetch_tle_of_observation
from skyfield.api import EarthSatellite, utc, load
import livejson

scheduler = BackgroundScheduler()
app = Flask(__name__)

ts = load.timescale()

Stations = []
Transmitters = defaultdict(dict)
SatDescrip = defaultdict(str)
Sats = {}
CZMLS = json.load(open("CZMLS.json"))
totalRequests = set()
temp = json.load(open("totalRequests.json"))
for x in temp:
    totalRequests.update([tuple(x)])

requested = []




def GetGroundStations():
    print("Getting Ground Stations")
    stations = get_paginated_endpoint("https://network.satnogs.org/api/stations/")
    return stations

def getSats():
    sats = get_paginated_endpoint("https://db.satnogs.org/api/satellites/")
    out = {}
    for x in sats:
        out[x["norad_cat_id"]] = x["name"]
    return out


@scheduler.scheduled_job('interval', days=5)
def updateTransmitters():
    global Transmitters
    print("Updating Transmitters")
    temp = requests.get("https://db.satnogs.org/api/transmitters/").json()
    for x in tqdm(temp):
        Transmitters[x["norad_cat_id"]][x["uuid"]] = [x["description"], [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), 255]]

    for x in Transmitters.keys():
        for y in Transmitters[x].keys():
            SatDescrip[x] += '<div class="trans" style="background-color:#'+str("%02x" % Transmitters[x][y][1][0])+str("%02x" % Transmitters[x][y][1][1])+str("%02x" % Transmitters[x][y][1][2])+'";>'+Transmitters[x][y][0]+'</div>'




@scheduler.scheduled_job('interval', hours=1)
def updateStations():
    global Stations
    print("Updating Stations")
    Stations = GetGroundStations()



@app.route("/", methods=['GET', 'POST'])
def index():
    end = request.args.get('end-time', default = '', type = str) 
    start = request.args.get('start-time', default = '', type = str)
    norad = request.args.get('norad', default = '', type = str)
    decript = request.args.get('descrip', default = '', type = str)
    if end == '' or start == '' or norad == '':
        return "<h3> Please supply the url with args. Copy them from the observations page URL after setting the Start, End and Noard.</h3>"
    index = hashlib.md5((end+start+norad).encode()).hexdigest()
    if index in CZMLS.keys():
        return '<h3> Thank you for requesting a display. Once it is done processing it will be available  <a href="/view?request='+index+'">HERE</a><h3>'
    requested.append((end,start,norad,index))
    totalRequests.update([(end,start,norad,index,decript)])
    return '<h3> Thank you for requesting a display. Once it is done processing it will be available  <a href="/view?request='+index+'">HERE</a><h3>'
        
def czml_gen(x):
    global CZMLS
    end,start,norad,index = x
    try:
        CZMLS[index] = api_station(get_paginated_endpoint("https://network.satnogs.org/api/observations/?end="+end+"&start="+start+"&satellite__norad_cat_id="+norad))
    except Exception as e:
        print(e)

@app.route("/view")
def view():
    index = request.args.get('request',default = "",type=str)
    return render_template("view.html",url="/czml?request="+index)
        
@app.route("/all_requests")
def all_requests():
    temp = {}
    for x in totalRequests:
        temp[x[3]] = x[3] in CZMLS.keys()

    return render_template("status.html",totalRequests = list(totalRequests), status = temp)

@app.route("/czml")
def api_czml():
    index = request.args.get('request',default = "",type=str)
    if index not in CZMLS:
        czml = []
        doc = {}
        doc["id"] = "document"
        doc["name"] = "sats"
        doc["version"] = "1.0"
        doc["clock"] = {}
        doc["clock"]["step"] = "SYSTEM_CLOCK"
        czml.append(doc)
        return json.dumps(czml)
   ## open("Test.json","w+").write(str(CZMLS[index]))
    return json.dumps(CZMLS[index])

@app.route("/force")
def force():

    processRequests()
    return "Done"

def api_station(data):
    totalNetwork = 0
    czml = []
    doc = {}
    doc["id"] = "document"
    doc["name"] = "sats"
    doc["version"] = "1.0"
    doc["clock"] = {}
    doc["clock"]["interval"] = data[-1]["end"]+"/"+data[0]["start"]
    doc["clock"]["currentTime"] =  data[-1]["end"]
    doc["clock"]["step"] = "SYSTEM_CLOCK"
    doc["clock"]["range"] = "CLAMPED"
    czml.append(doc)

    for x in tqdm(Stations):
        #print x
        color = [0, 230, 64, 255]
        if x["status"] == "Testing":
            color = [248, 148, 6, 255]
        if x["status"] == "Offline":
             color = [255, 0, 0, 50]
        station = {}
        station["id"] = str(x["id"])
        station["name"] = x["name"]
        station["point"] = {}
        station["show"] = True
        station["point"]["color"] = {}
        station["point"]["color"]["rgba"] = color
        station["point"]["outlineColor"] = {}
        station["point"]["outlineColor"]["rgba"] = [255, 255, 255, color[3]]
        station["point"]["outlineWidth"] = 2.0
        station["position"] = {}
        station["point"]["pixelSize"] = 7.0
        station["position"]["cartographicDegrees"] = [x["lng"], x['lat'], x["altitude"]]
        station["description"] = "<b>ID: " + str(x["id"]) + "</b><br><b>Total Observations: "
        station["description"] += str(x["observations"]) + "</b><br><b>Status: " + x["status"] + "</b><br><b>QTH: "
        station["description"] += x["qthlocator"] + "</b><br></b>Description: </b>" + x["description"]
        czml.append(station)
        
    TLE = None
    for y in tqdm(data):
        sat = {}
        sat["id"] = str(y["id"])
        sat["name"] = Sats[y["norad_cat_id"]]
        sat["show"] = True
        sat["point"] = {}
        sat["point"]["color"] = {}
        sat["point"]["color"]["rgba"] = [255, 0, 0, 255]
        sat["point"]["pixelSize"] = 12.0
        sat["position"] = {}
        sat["position"]["cartographicDegrees"] = []
        sat["description"] = SatDescrip[y["norad_cat_id"]]
        temp = datetime.strptime(y["start"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc)
        if TLE == None:
            TLE = fetch_tle_of_observation(y["id"])
            totalNetwork+=1
        
        satObj = EarthSatellite(TLE[0],TLE[1])
        if (ts.utc(temp) - satObj.epoch) > 14:
            totalNetwork+=1
            TLE = fetch_tle_of_observation(y["id"])
        time = 0
        while temp <= datetime.strptime(y["end"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc)+timedelta(minutes=1):
            subpoint = satObj.at(ts.utc(temp)).subpoint()
            lat = subpoint.latitude.degrees
            lng = subpoint.longitude.degrees
            elevation = subpoint.elevation.m
            sat["position"]["cartographicDegrees"].extend([time, lng, lat, elevation])
            time += 60
            temp = temp + timedelta(minutes=1)
        sat["position"]["interpolationAlgorithm"] = "LAGRANGE"
        sat["position"]["interpolationDegree"] = 5
        sat["position"]["epoch"] = (datetime.strptime(y["start"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc).isoformat() + "Z").replace("+00:00", "")
        sat["path"] = {"show": {"interval": (datetime.strptime(y["start"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc).isoformat()+"Z").replace("+00:00", "") + "/" + ((datetime.strptime(y["end"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc) + timedelta(minutes=1)).isoformat() + "Z").replace("+00:00", ""), "boolean": True}, "width": 2, "material": {"solidColor": {"color": {"rgba": [0, 255, 0, 255]}}}, "leadTime": 100000, "trailTime": 100000}
        czml.append(sat)
    for y in data:
        if not y["station_name"] == None:
            sat = {}
            sat["id"] = str(y["id"])+"Link"
            if y["vetted_status"] == "good":
                sat["polyline"] = {"show": {"interval": (datetime.strptime(y["start"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc).isoformat()+"Z").replace("+00:00", "") + "/" + ((datetime.strptime(y["end"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc) + timedelta(minutes=1)).isoformat() + "Z").replace("+00:00", ""), "boolean": True}, "width": 2, "material": {"solidColor": {"color": {"rgba": [0,255,0,255]}}}, "followSurface": False, "positions": {"references": [str(y["id"]) + "#position", str(y["ground_station"]) + "#position"]}}
            else:
                sat["polyline"] = {"show": {"interval": (datetime.strptime(y["start"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc).isoformat()+"Z").replace("+00:00", "") + "/" + ((datetime.strptime(y["end"], '%Y-%m-%dT%H:%M:%Sz').replace(tzinfo=utc) + timedelta(minutes=1)).isoformat() + "Z").replace("+00:00", ""), "boolean": True}, "width": 2, "material": {"solidColor": {"color": {"rgba": [255,0,0,255]}}}, "followSurface": False, "positions": {"references": [str(y["id"]) + "#position", str(y["ground_station"]) + "#position"]}}
            czml.append(sat)
    print(totalNetwork)
    return czml

@scheduler.scheduled_job('interval', minutes=5)
def processRequests():
    global requested
    try:
        for x in range(len(requested)):
            czml_gen(requested.pop())
    except Exception as e:
        print(e)
    os.remove("CZMLS-bak.json")
    os.rename("CZMLS.json","CZMLS-bak.json")
    temp = open("CZMLS.json","w+")
    temp.write(json.dumps(CZMLS))
    temp.close()
    os.remove("totalRequests-bak.json")
    os.rename("totalRequests.json","totalRequests-bak.json")
    temp = open("totalRequests.json","w+")
    temp.write(json.dumps(list(totalRequests)))
    temp.close()

    
updateStations()
#updateTransmitters()
Sats = getSats()
#getFuture()
#updateCZML()
scheduler.start()
app.run(use_reloader=False, host="0.0.0.0", port=5001)
